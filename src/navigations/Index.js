import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Auth/Login';
import Register from '../screens/Auth/Register';
import ChatScreen from '../screens/Chat/ChatScreen';
import DashboardUser from '../screens/Chat/DashboardUser';
import ChatList from '../screens/Chat/ChatList';
import ProfileUser from '../screens/Chat/ProfileUser';

const Stack = createStackNavigator();

export default function Index() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        detachInactiveScreens={false}
        initialRouteName="Auth"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="ChatList" component={ChatList} />
        <Stack.Screen name="ProfileUser" component={ProfileUser} />
        <Stack.Screen name="DashboardUser" component={DashboardUser} />
        <Stack.Screen name="ChatScreen" component={ChatScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
