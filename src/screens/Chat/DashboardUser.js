import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  StyleSheet,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {COLORS} from '../../utils/COLORS';
import {firebase} from '@react-native-firebase/database';
import DashboardUserHeader from '../../components/Chat/DashboardUser/DashboardUserHeader';
import Search from '../../components/Chat/Search';
import uuid from 'react-native-uuid';

export default function DashboardUser({route, navigation}) {
  const [search, setSearch] = useState('');
  const [allUser, setAllUser] = useState([]);
  const [allUserBackup, setAllUserBackup] = useState([]);

  const userData = route.params.userData;

  useEffect(() => {
    getAllUser();
  }, []);

  const searchUser = val => {
    setSearch(val);
    setAllUser(allUserBackup.filter(it => it.name.match(val)));
  };

  const myDB = firebase
    .app()
    .database(
      'https://chatappch7-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const getAllUser = () => {
    myDB
      .ref('users/')
      .once('value')
      .then(snapshot => {
        setAllUser(
          Object.values(snapshot.val()).filter(it => it.id != userData.id),
        );
        setAllUserBackup(
          Object.values(snapshot.val()).filter(it => it.id != userData.id),
        );
      });
  };

  const createChatList = data => {
    myDB
      .ref('/chatlist/' + userData.id + '/' + data.id)
      .once('value')
      .then(snapshot => {
        if (snapshot.val() == null) {
          let roomId = uuid.v4();
          let myData = {
            roomId,
            id: userData.id,
            name: userData.name,
            emailId: userData.emailId,
            bio: userData.bio,
            lastMsg: '',
          };
          myDB.ref('/chatlist/' + data.id + '/' + userData.id).update(myData);

          delete data['password'];
          data.lastMsg = '';
          data.roomId = roomId;
          myDB.ref('/chatlist/' + userData.id + '/' + data.id).update(data);

          navigation.navigate('ChatScreen', {
            receiverData: data,
            userData: userData,
          });
        } else {
          navigation.navigate('ChatScreen', {
            receiverData: snapshot.val(),
            userData: userData,
          });
        }
      });
  };

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.FlatListContainer}
        onPress={() => createChatList(item)}>
        <Image
          style={styles.Image}
          source={{
            uri: 'https://cdn-icons-png.flaticon.com/512/147/147142.png',
          }}
        />
        <View>
          <Text style={styles.Name}>{item.name}</Text>
          <Text style={styles.Bio}>{item.bio}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.Container}>
      <DashboardUserHeader navigation={navigation} userData={userData} />
      <Search onChangeText={val => searchUser(val)} value={search} />
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        data={allUser.sort((a, b) => a.name.localeCompare(b.name))}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
    paddingBottom: 40,
  },
  FlatListContainer: {
    backgroundColor: COLORS.card,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 40,
    height: 75,
    marginBottom: 20,
    borderRadius: 18,
  },
  Image: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginHorizontal: 20,
  },
  Name: {
    fontFamily: 'Brandon_bld',
    fontSize: 16,
    color: COLORS.text,
  },
  Bio: {
    fontFamily: 'Brandon_med',
    fontSize: 14,
    color: COLORS.text,
  },
});
