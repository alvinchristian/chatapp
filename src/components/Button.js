import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {COLORS} from '../utils/COLORS';

const Button = ({caption, onPress}) => {
  return (
    <TouchableOpacity style={styles.Container} onPress={onPress}>
      <Text style={styles.Text}>{caption}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.button,
    width: '80%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 18,
    marginTop: 20,
  },
  Text: {
    fontFamily: 'Brandon_blk',
    fontSize: 16,
    color: COLORS.white,
  },
});

export default Button;
