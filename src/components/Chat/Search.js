import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../utils/COLORS';

export default function Search({onChangeText, value}) {
  return (
    <View style={styles.Container}>
      <Icon
        style={styles.Icon}
        name={'magnify'}
        size={22}
        color={COLORS.icon}
      />
      <TextInput
        placeholder="Search"
        onChangeText={onChangeText}
        value={value}
        style={styles.Input}
        placeholderTextColor={COLORS.placeholder}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.textInput,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 20,
    paddingHorizontal: 20,
    borderRadius: 15,
    height: 50,
  },
  Icon: {
    marginRight: 8,
  },
  Input: {
    fontSize: 15,
    fontFamily: 'Brandon_med',
  },
});
