// import moment from 'moment';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {COLORS} from '../../../utils/COLORS';
import TimeDelivery from './TImeDelivery';

const MsgComponent = ({sender, item}) => {
  return (
    <View style={{marginVertical: 0}}>
      <View
        style={{
          ...styles.msgBox,
          alignSelf: sender ? 'flex-end' : 'flex-start',
          backgroundColor: sender ? COLORS.msgBox : COLORS.msgBox2,
        }}>
        <Text
          style={{
            ...styles.Message,
            color: sender ? COLORS.white : COLORS.black,
          }}>
          {item.message}
        </Text>
        <TimeDelivery sender={sender} item={item} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  msgBox: {
    marginHorizontal: 10,
    minWidth: 50,
    maxWidth: '80%',
    justifyContent: 'center',
    marginBottom: 20,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  Message: {
    fontFamily: 'Brandon_blk',
    fontSize: 16,
    textAlign: 'justify',
  },
});

export default MsgComponent;
