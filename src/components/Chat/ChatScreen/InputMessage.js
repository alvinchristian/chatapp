import React from 'react';
import {View, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../../utils/COLORS';

export default function InputMessage({value, onChangeText, disabled, onPress}) {
  return (
    <View style={styles.Container}>
      <TextInput
        style={styles.Input}
        placeholder="Type a message.."
        placeholderTextColor={COLORS.placeholder}
        multiline={true}
        value={value}
        onChangeText={onChangeText}
      />

      <TouchableOpacity
        style={styles.ButtonBox}
        disabled={disabled}
        onPress={onPress}>
        <Icon style={styles.Icon} name="send" size={25} color={COLORS.white} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.backgroundColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  Input: {
    backgroundColor: COLORS.textInput,
    width: '75%',
    borderRadius: 15,
    paddingHorizontal: 20,
    paddingVertical: 20,
    color: COLORS.text,
    fontSize: 15,
    fontFamily: 'Brandon_bld',
  },
  ButtonBox: {
    backgroundColor: COLORS.button,
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderRadius: 15,
  },
});
