//import liraries
import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../../utils/COLORS';

const DashboardUserHeader = ({navigation, userData}) => {
  return (
    <View style={styles.Container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('ChatList', {userData: userData})}>
        <Icon
          style={styles.Icon}
          name={'keyboard-backspace'}
          size={25}
          color={COLORS.icon}
        />
      </TouchableOpacity>
      <Text style={styles.Title}>Profile User</Text>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.backgroundColor,
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 25,
    alignSelf: 'flex-start',
  },
  Title: {
    fontFamily: 'Brandon_blk',
    fontSize: 25,
    color: COLORS.text,
    textAlign: 'center',
    width: '85%',
  },
});

//make this component available to the app
export default DashboardUserHeader;
