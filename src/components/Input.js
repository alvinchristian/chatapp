import React, {useState} from 'react';
import {View, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../utils/COLORS';

const Input = ({iconName, placeholder, onChangeText, secureTextEntry}) => {
  const [isSecureText, setIsSecureText] = useState(secureTextEntry);
  const [icon, setIcon] = useState('eye-off-outline');
  return (
    <View style={styles.Container}>
      <Icon style={styles.Icon} name={iconName} size={22} color={COLORS.icon} />
      <TextInput
        style={styles.Input}
        placeholder={placeholder}
        onChangeText={onChangeText}
        secureTextEntry={isSecureText}
        placeholderTextColor={COLORS.placeholder}
      />
      {placeholder == 'Password' ? (
        <TouchableOpacity
          onPress={() => {
            setIsSecureText(prev => !prev);
            {
              isSecureText
                ? setIcon('eye-outline')
                : setIcon('eye-off-outline');
            }
          }}>
          <Icon
            style={styles.Icon2}
            name={icon}
            size={20}
            color={COLORS.icon}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.textInput,
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 18,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginBottom: 15,
  },
  Icon: {
    marginVertical: 10,
    marginLeft: 10,
    paddingRight: 10,
    borderRightWidth: 2,
    borderColor: COLORS.white,
  },
  Input: {
    fontFamily: 'Brandon_med',
    marginHorizontal: 5,
    width: '68%',
    fontSize: 15,
  },
  Icon2: {
    marginVertical: 10,
    paddingLeft: 5,
  },
});

export default Input;
